import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

import Books from './pages/panel/Books'
import Patron from './pages/panel/Patron'
import Dashboard from './pages/panel/Dashboard'
import Settings from './pages/panel/Settings'
import Index from './pages/panel/Index'
import Login from './pages/Login'
import Librarian from './pages/panel/Librarian'
import BorrowedBooks from './pages/panel/BorrowedBooks'
import ReturnedBooks from './pages/panel/ReturnedBooks'

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { hasUser: true }
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('./pages/Register'),
    meta: { hasUser: true }
  },
  {
    path: '/manage',
    name: 'Manage',
    component: Index,
    meta: {requiresLogin: true },
    children: [
      {
        path: 'books',
        name: 'books',
        components: {
          books: Books
        }
      },
      {
        path: 'borrowed',
        name: 'borrowed',
        components: {
          borrowed: BorrowedBooks
        }
      },
      {
        path: 'returned',
        name: 'returned',
        components: {
          returned: ReturnedBooks
        }
      },
      {
        path: 'patron',
        name: 'patron',
        components: {
          patron: Patron
        }
      },
      {
        path: 'settings',
        name: 'settings',
        components: {
          settings: Settings
        }
      },
      {
        path: 'librarian',
        name: 'librarian',
        components: {
          librarian: Librarian
        }
      },
      {
        path: 'dashboard',
        name: 'dashboard',
        components: {
          dashboard: Dashboard
        }
      },
    ]
  },
  {
    path: '*',
    name: 'notfound',
    component: () => import(/* webpackChunkName: "notfound" */ './pages/panel/NotFound'),
  }
]

const router = new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
	if (to.matched.some((record) => record.meta.requiresLogin) && !localStorage.getItem('auth')){
    next({name: 'Login'})
  }
  else if (to.matched.some((record) => record.meta.hasUser) && localStorage.getItem("auth")) {
      next({ name: "dashboard" });
	} else {
		next();
	}
});

export default router;