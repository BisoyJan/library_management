import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

export default ({
  namespaced: true,
  state: {
    librarian: [],
  },
  actions: {
    async getLibrarianAccounts({commit}, page){
      const res = await AXIOS.get(`librarian/accounts?page=${page}`).then(response => {
        commit('SET_DATA', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async searchLibrarian({commit}, {data, page}){
      const res = await AXIOS.post(`librarian/searchLibrarian?page=${page}`,data).then(response => {
        commit('SET_DATA', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async setAccount({commit}, id){
      const res = await AXIOS.post('setType', id).then(response => {
       commit('SET_DATA', response.data)

        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {

  },
  mutations: {
   SET_DATA(state, data) {
    state.librarian = data
   }
  },
})