import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const patron = 'patrons'

export default ({
  namespaced: true,
  state: {
    patrons: [],
    allpatrons: [],
  },
  actions: {
    async getAllPatrons({commit}){
      const res = await AXIOS.get(`all/patron`)
      .then(response => {
        commit('SET_ALL_PATRONS', response.data);
        return response;
      })
      .catch(error => {
        return error.response;
      });
      
      return res;
    },
    async getPatrons({commit}, page){
      const res = await AXIOS.get(`${patron}?page=${page}`)
      .then(response => {
        commit('SET_PATRON', response.data);
        return response;
      })
      .catch(error => {
        return error.response;
      });
      
      return res;
    },
    async storePatron({commit},data){
      const res = await AXIOS.post(`${patron}`, data)
      .then(response => {
        commit('SAVE_PATRON', response.data)
        return response;
      })
      .catch(error => {
        return error.response;
      });
      
      return res;
    },
    async deletePatron({commit}, id){
      const res = await AXIOS.delete(`${patron}/${id}`)
      .then(response => {
        commit('DELETE_PATRON', id)
        return response;
      })
      .catch(error => {
        return error.response;
      });
      
      return res;
    },
    async updatePatron({commit}, {data, index}){
      console.log('UPDATE', data, 'INDEX', index)
      const res = await AXIOS.put(`${patron}/${data.id}`, data)
      .then(response => {
        commit('UPDATE_PATRON', {index, data})
        return response;
      })
      .catch(error => {
        return error.response;
      });
     
      return res;
    },
    async searchPatrons({commit}, {data, page}){
      const res = await AXIOS.post(`${patron}/searchPatron?page=${page}`,data).then(response => {
        commit('SET_PATRON', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },

  getters: {
    getPatronData: function(state) {
      return state.patrons;
    }
  },
  mutations: {
    SET_PATRON(state, data){
      state.patrons = data;
    },
    SET_ALL_PATRONS(state, data){
      state.allpatrons = data;
    },
    SAVE_PATRON(state, data){
      state.patrons.data.unshift(data)
    },
    DELETE_PATRON(state, id){
      state.patrons.data = state.patrons.data.filter(patron => {
        return patron.id !== id
      })
    },
    UPDATE_PATRON(state, {index, data}){
      Vue.set(state.patrons.data, index, data);
    }
  },
})