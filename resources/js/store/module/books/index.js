import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);

const CATEGORY = 'categories'
const BOOKS = 'books'

export default ({
  namespaced: true,
  state: {
    books: [],
    categories: [],
  },
  actions: {
    async getCategories({commit}){
      const res = await AXIOS.get(CATEGORY).then(response => {
        commit('SET_CATEGORIES', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async getBooks({commit}, page){
      const res = await AXIOS.get(`${BOOKS}?page=${page}`).then(response => {
        commit('SET_BOOKS', response.data)
        return response
      }).catch(error => {
        return error.response;
      })
      return res
    },
    async searchBook({commit}, {data, page}){
      const res = await AXIOS.post(`${BOOKS}/searchBook?page=${page}`, data).then(response => {
        commit('SET_BOOKS', response.data)
        return response
      }).catch(error => {
        return error.response;
      })
      return res
    },
    async storeBook({commit}, book){
      const res = await AXIOS.post(`${BOOKS}`, book).then(response => {
        commit('SAVE_BOOK', response.data);
        return response
      }).catch(error => {
        return error.response
      });
      
      return res;
    },
    async updateBook({commit}, {index, data}){
      const res = await AXIOS.put(`${BOOKS}/${data.id}`, data).then(response => {
        commit('UPDATE_BOOK', {id: index, data: response.data.book});
        return response;
      }).catch(error => {
        return error.response
      });

      return res;
    },
    async deleteBook({commit}, id){
      const res = await AXIOS.delete(`${BOOKS}/${id}`).then(response => {
        commit('DELETE_BOOK', id);
        return response;
      });
      return res;
    }
  },
  getters: {
    getBooksData(state){
      return state.books;
    }
  },
  mutations: {
    SET_CATEGORIES(state, categories){
      state.categories = categories
    },
    SET_BOOKS(state, books){
      state.books = books;
    },
    DELETE_BOOK(state, id){
      state.books.data = state.books.data.filter(book => {
        return book.id !== id;
      });
    },
    UPDATE_BOOK(state, {id, data}){
      Vue.set(state.books.data, id, data);
    },
    SAVE_BOOK(state, book){
      state.books.data.push(book);
    },
    UPDATE_COPIES(state, {id, data}){
      state.books.data[id].copies = data
    }
  },
})