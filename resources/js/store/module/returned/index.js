import Vue from 'vue'
import Vuex from 'vuex'
import API from '../../base/config'

Vue.use(Vuex);

const returnedbooks = 'returnedbook'

export default ({
  namespaced: true,
  state: {
    returnedbooks: [],
    summary: [],
  },
  actions: {
    async getReturnedBooks({commit}, page){
      const res = await API.get(`${returnedbooks}?page=${page}`).then(response => {
        commit('SET_RETURNED', response.data)
        return response;
      }).catch(error => {
        return error.response;
      })

      return res;
    },
    async searchReturned({commit}, {data, page}){
      const res = await API.post(`${returnedbooks}/searchReturned?page=${page}`, data).then(response => {
        commit('SET_RETURNED', response.data)
        return response;
      }).catch(error => {
        return error.response;
      })

      return res;
    },
    async getReturnedSummary({commit}){
      const res = await API.get(`returnedsummary`).then(res => {
        commit('SET_SUMMARY', res.data)
        return res
        
      })
      .catch(error => {
        return error.response
      })

      return res;
    }
  },
  getters: {

  },
  mutations: {
    SET_RETURNED(state, data) {
      state.returnedbooks = data
    },
    SET_SUMMARY(state, data){
     state.summary = data
    }
  },
})