import Vue from 'vue'
import Vuex from 'vuex'
import API from '../../base/config'

Vue.use(Vuex);

const borrowedbook = 'borrowedbook'

export default ({
  namespaced: true,
  state: {
    borrowedbooks: [],
    summary: [],
  },
  actions: {
    async storeBorrowed({commit}, {data, index}){
      const res = await API.post(`${borrowedbook}`, data)
      .then(response => {
        commit('books/UPDATE_COPIES', {id: index, data: response.data.borrowedbook.book.copies}, {root: true})
        return response
      })
      .catch(error => {
        return error.response
      })

      return res;
    },
    async getBorrowed({commit}, page){
      const res = await API.get(`${borrowedbook}?page=${page}`).then(response => {
        commit('SET_BORROWED', response.data)
        return response;
      }).catch(error => {
        return error.response;
      })

      return res;
    },
    async searchBorrowed({commit}, {data, page}){
      const res = await API.post(`${borrowedbook}/searchBorrowed?page=${page}`, data).then(response => {
        commit('SET_BORROWED', response.data)
        return response;
      }).catch(error => {
        return error.response;
      })

      return res;
    },
    async returnBorrowedBook({commit}, {data, index}){
      const res = await API.post('returnedbook', data).then(res => {
        if(res.data.book != null) {
          commit('UPDATE_BORROWED_COPIES', {id: index, data: res.data.book.copies})
        }
        else if(res.data.book == null) {
          commit('UPDATE_BORROWED_COPIES', {id: index, data: null})
        }
        return res
        
      })
      .catch(error => {
        return error.response
      })

      return res;
    },
    async getBorrowedSummary({commit}){
      const res = await API.get(`borrowedsummary`).then(res => {
        commit('SET_SUMMARY', res.data)
        return res
        
      })
      .catch(error => {
        return error.response
      })

      return res;
    }
  },
  getters: {

  },
  mutations: {
    SET_BORROWED(state, data) {
      state.borrowedbooks = data
    },
    UPDATE_BORROWED_COPIES(state, {id, data}){
      if(data == null) {
        state.borrowedbooks.data.splice(id, 1)
      }else {
        state.borrowedbooks.data[id].copies = data
      }
    },
    SET_SUMMARY(state, data){
      state.summary = data
    }
  },
})