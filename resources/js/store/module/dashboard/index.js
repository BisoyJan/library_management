import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../../base/config'

Vue.use(Vuex);


export default ({
  namespaced: true,
  state: {
    data: [],
    chart: [],
  },
  actions: {
    async getLatestData({commit}){
      const res = await AXIOS.get('latest').then(response => {
        commit('SET_DATA', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
    async getChartData({commit}){
      const res = await AXIOS.get('latest/chart').then(response => {
        commit('SET_CHART', response.data)
        return response
      }).catch(error => {
        return error.response
      });
    
      return res;
    },
  },
  getters: {

  },
  mutations: {
    SET_DATA(state, data){
     state.data = data
    },
    SET_CHART(state, data){
      state.chart = data
    }
  },
})