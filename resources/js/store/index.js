import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

import auth from "./module/auth/index";
import books from "./module/books/index";
import patrons from "./module/patron/index";
import borrowed from "./module/borrowed/index";
import returned from "./module/returned/index";
import librarian from "./module/librarian/index";
import dashboard from "./module/dashboard/index";

export default new Vuex.Store({
  modules: {
    auth, books, dashboard, borrowed, patrons, librarian, returned
  }
})