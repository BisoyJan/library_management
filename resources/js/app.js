require('./bootstrap');
window.Vue = require('vue').default;

import router from './router.js'
import store from './store/index'
import Toast from "vue-toastification";

import "vue-toastification/dist/index.css";
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.component('main-component', () => import('./App.vue'));
Vue.component('chart-component', require('./components/charts/BarChart.vue').default);
Vue.component('polarchart-component', require('./components/charts/PolarChart.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(BootstrapVue)
Vue.use(Toast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 4,
    hideProgressBar: true,
});

const app = new Vue({
    el: '#app',
    router,
    store
});
