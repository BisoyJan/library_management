<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <title>Library Management System</title>
    </head>
    <body>
        <div id="app">
           <main-component></main-component>
        </div>
    </body>
    <script src="{{ mix('/js/app.js') }}"></script>
</html>
