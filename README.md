# LIBRARY MANAGEMENT
## Members
<ul><li>Genreve Fernandez (Project Manager)</li> 
<li>Yvan Sabay</li> 
<li>Jan Ramil Intong</li>
 <li>Danica Barrientos</li> 
 </ul>

### Yvan Sabay
### Jan Ramil Intong
### Danica Barrientos

## Requirements
### [Git](https://git-scm.com/downloads)
### [Docker](https://www.docker.com/products/docker-desktop)

## Installation
<ol>
<li>Clone the repository</li>
<li>Copy .env file (Windows Terminal: xcopy .env.example .env)</li>
<li>Open terminal on project directory and run <br/><code>docker-compose build</code></li>
<li>Install dependencies <br/><code>docker-compose run composer install</code><br/><code>docker-compose run npm install</code></li>
<li>Run docker <br/><code>docker-compose up -d</code></li>
<li>Generate APP_KEY <br/><code>php artisan key:generate</code></li>
<li>Generate JWT_SECRET <br/><code>php artisan jwt:secret</code></li>
<li>Set DB_HOST on .env to libsys_mysql</li>
<li>Migrate and seed database <br/><code>docker exec -it libsys_php php artisan migrate</code><br/><code>docker exec -it libsys_php php artisan db:seed</code></li>
<li>Access project on localhost:8080</li>
</ol>

## Admin Credentials
Email: <code>admin@admin.com</code><br/>
Password: <code>admin123</code>

