<?php

namespace Tests\Unit;
use Tests\TestCase;

class T1LaravelSetup extends TestCase
{

    public function test_laravel_can_access_home_route()
    {
        $this->get('/')->assertStatus(200);
    }
}