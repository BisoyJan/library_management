<?php

namespace Tests\Unit;

use App\Models\Patron;
use App\Models\User;
use Tests\TestCase;

class PatronRouteandApiTest extends TestCase
{

    public function authenticate_account()
    {
        $user = User::factory()->create();

        $account = [
            'email' => 'admin@admin.com',
            'password' => 'admin123',
        ];

        $response = $this->call('POST', 'api/auth/login?', $account);

        return $response;
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_access_get_all_patron()
    {
        $response = $this->authenticate_account();
        $this->call('GET', 'api/patrons', ['token' => $response['access_token']])->assertStatus(200);
    }

    public function test_can_create_patron()
    {
        $response = $this->authenticate_account();

        $patron = Patron::factory()->make();
        $patron['token'] = $response['access_token'];

        $res = $this->call('POST', '/api/patrons', $patron->toArray())->assertStatus(200);
        unset($patron['token']);
        $this->assertDatabaseHas('patrons', $patron->toArray());
    }

    public function test_can_update_patron()
    {
        $response = $this->authenticate_account();

        $patron = Patron::first();
        $data = [
            'last_name' => 'Sample',
            'first_name' => 'Patron',
            'middle_name' => 'Test',
            'email' => 'sabayyvan2018@gmail.com',
            'contact_number' => '09123456',
            'gender' => 'Male'
        ];
        $data['token'] = $response['access_token'];
        $response = $this->call('PUT', '/api/patrons/' . $patron->id, $data);
        unset($data['token']);
        $response->assertStatus(200);
        $this->assertDatabaseHas('patrons', $data);
    }

    public function test_can_delete_patron()
    {
        $response = $this->authenticate_account();

        $patron = Patron::factory()->make();

        $this->call('DELETE', 'api/patrons/' . $patron->id, [], ['token' => $response['access_token']]);

        $this->assertDatabaseMissing('patrons', $patron->toArray());
    }

    public function test_can_show_specific_patron()
    {
        $response = $this->authenticate_account();

        $patron = Patron::factory()->create();

        $this->call('GET', '/api/patrons/' . $patron->id, [], ['token' => $response['access_token']])->assertJson($patron->toArray());
    }
}
