<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryandBookRouteandApiTest extends TestCase
{

    public function authenticate_account()
    {
        $account = [
            'email' => 'admin@admin.com',
            'password' => 'admin123',
        ];

        $response = $this->call('POST', 'api/auth/login?', $account);

        return $response;
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function test_can_get_categories()
    {
        $response = $this->authenticate_account();

        $this->call('GET', 'api/categories', ['token' => $response['access_token']])->assertStatus(200);
    }

    public function test_can_access_get_all_book()
    {
        $response = $this->authenticate_account();

        $this->call('GET', 'api/books', ['token' => $response['access_token']])->assertStatus(200);
    }

    public function test_can_create_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Programming']);
        $book = Book::factory()->make(['category_id' => $category->id]);
        $book['token'] = $response['access_token'];

        $this->call('POST', '/api/books', $book->toArray());

        unset($book['token']);

        $this->assertDatabaseHas('books', $book->toArray());
    }

    public function test_can_update_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Programming - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);
        $data = ['name' => 'PHP Basics - Updated', 'copies' => 15, 'category_id' => $category->id, 'author' => 'Sabay', 'description' => 'This is a sample desc'];
        $data['token'] = $response['access_token'];
        $expected_result = ['message' => 'Book updated successfully!'];

        $res = $this->call('PUT', '/api/books/' . $book->id, $data);
        unset($data['token']);

        $this->assertDatabaseHas('books', $data);
    }

    public function test_can_delete_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Programming - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('DELETE', '/api/books/' . $book->id, ['token' => $response['access_token']]);
        $this->assertDatabaseMissing('books', $book->toArray());
    }

    public function test_can_show_specific_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Programming - 2']);
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->call('GET', '/api/books/' . $book->id, ['token' => $response['access_token']])->assertJson($book->toArray());
    }
}
