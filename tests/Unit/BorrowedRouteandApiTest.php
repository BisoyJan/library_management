<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BorrowedRouteandApiTest extends TestCase
{

    public function authenticate_account()
    {
        $account = [
            'email' => 'admin@admin.com',
            'password' => 'admin123',
        ];

        $response = $this->call('POST', 'api/auth/login?', $account);

        return $response;
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_can_access_route_and_store_borrowed_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Sci-fi']);
        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();
        $borrowed = BorrowedBook::make(['copies' => 5, 'book_id' => $book->id, 'patron_id' => $patron->id, 'returndate' => '2021-07-19']);
        $borrowed['token'] = $response['access_token'];

        $this->call('POST', '/api/borrowedbook', $borrowed->toArray())->assertStatus(200);
        unset($borrowed['token']);
        $this->assertDatabaseHas('borrowed_books', $borrowed->toArray());
    }

    public function test_can_get_all_borrowed()
    {
        $response = $this->authenticate_account();

        $this->call('GET', '/api/borrowedbook', ['token' => $response['access_token']])->assertSuccessful();
    }

    public function test_can_get_specific_borrowed_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Biography']);
        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();
        $borrowed = BorrowedBook::create(['copies' => 12, 'book_id' => $book->id, 'patron_id' => $patron->id, 'returndate' => '2021-07-09']);

        $this->call('GET', '/api/borrowedbook/' . $borrowed->id, ['token' => $response['access_token']])
            ->assertJsonFragment($borrowed->toArray());
    }
}
