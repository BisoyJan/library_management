<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use App\Models\ReturnedBook;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ReturnedRouteandApiTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function authenticate_account()
    {
        $user = User::factory()->create();

        $account = [
            'email' => 'admin@admin.com',
            'password' => 'admin123',
        ];

        $response = $this->call('POST', 'api/auth/login?', $account);

        return $response;
    }

    public function test_can_store_returned_book()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Sci-fi']);
        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();

        $borrowed = BorrowedBook::create(['copies' => 5, 'book_id' => $book->id, 'patron_id' => $patron->id, 'returndate' => '2021-07-09']);
        $returned = ReturnedBook::create(['copies' => 5, 'book_id' => $book->id, 'patron_id' => $patron->id, 'borroweddate' => $borrowed->returndate]);

        $this->call('POST', '/api/returnedbook', $returned->toArray(), ['token' => $response['access_token']]);
        $this->assertDatabaseHas('returned_books', $returned->toArray());
    }

    public function test_can_get_all_returned()
    {
        $response = $this->authenticate_account();

        $this->call('GET', '/api/returnedbook', [], ['token' => $response['access_token']])->assertSuccessful();
    }

    public function test_can_get_specific_borrowed()
    {
        $response = $this->authenticate_account();

        $category = Category::create(['category' => 'Biography']);
        $patron = Patron::factory()->create();
        $book = Book::factory(['category_id' => $category->id])->create();
        $returned = ReturnedBook::create(['copies' => 12, 'book_id' => $book->id, 'patron_id' => $patron->id, 'borroweddate' => '2021-09-08']);

        $this->call('GET', '/api/returnedbook/' . $returned->id, [], ['token' => $response['access_token']])
            ->assertJsonFragment($returned->toArray());
    }
}
