<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        return response()->json(Book::with(['category:id,category'])->paginate(10));
    }

    public function searchBook(Request $request){
        $book = Book::whereHas('category', function ($query) {
            $query->where('category', 'like', '%'.request()->get('search').'%');
        })->orWhere('name', 'like', '%'.request()->get('search').'%')
        ->orWhere('description', 'like', '%'.request()->get('search').'%')
        ->orWhere('author', 'like', '%'.request()->get('search').'%')
        ->with(['category:id,category'])->paginate(10);

        return response()->json($book, 200);
    }

    public function store(BookRequest $request)
    {
        $storedbook = Book::create($request->validated());
        $book = Book::with(['category:id,category'])->find($storedbook->id);
        return response()->json($book);
    }

    public function show($id)
    {
        try {
            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            return response()->json($book);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

    public function update(BookRequest $request, $id)
    {
        try {
            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            $book->update($request->validated());

            $updated_book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            return response()->json(['message' => 'Book updated successfully!', 'book' => $updated_book]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

    public function destroy($id)
    {
        try {
            $book = Book::where('id', $id)->firstOrFail();
            $book->delete();

            return response()->json(['message' => 'Book deleted successfully!']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

}
