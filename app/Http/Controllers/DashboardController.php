<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
        
    public function getLatestData()
    {
        $borrowed = BorrowedBook::with(['book', 'patron'])->latest()->take(5)->get();
        $book = Book::with(['category:id,category'])->latest()->take(3)->get();
        $book_count = Book::count();
        $patron_count = Patron::count();
        $borrowed_count = BorrowedBook::sum('copies');
        $most_borrowed = BorrowedBook::with(['book'])->latest()->take(5)->get();
        $book_per_category = Category::with(['books'])->withCount('books')->get();
        $librarian = User::where('type', '<>', 'Admin')->count();

        return response()->json(['librarian' => $librarian, 'mostborrowed' => $most_borrowed, 'book' => $book, 'borrowed' => $borrowed, 'book_count' => $book_count, 'patron_count' => $patron_count, 'borrowed_count' => $borrowed_count, 'books_per_category' => $book_per_category]);
    }
}
