<?php

namespace App\Http\Controllers;

use App\Http\Requests\BorrowedBookRequest;
use App\Models\BorrowedBook;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BorrowedBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        return response()->json(BorrowedBook::with([
            'patron', 'book', 'book.category'
        ])->paginate(10));
    }

    public function filterBorrowed(Request $request)
    {
        $borrowed = BorrowedBook::whereBetween('returndate', [$request->daterange[0], $request->daterange[1]])->with(['patron', 'book', 'book.category'])->paginate(10);
        return response()->json($borrowed);
    }

    public function show($id)
    {
        try {
            $borrowedbook = BorrowedBook::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail();
            return response()->json($borrowedbook);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Borrowed book not found'], 404);
        }
    }

    public function store(BorrowedBookRequest $request)
    {
        //Store to database
        $create_borrowed = BorrowedBook::create($request->validated());

        //Retrieve from db and update book copy
        $borrowedbook = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbook]);
    }

    public function summary(){

        $due = BorrowedBook::whereDate('returndate', Carbon::now('Asia/Manila')->format('Y-m-d'))->count();
        $totalborrowed = BorrowedBook::count();
        return response()->json(['due' => $due, 'totalborrowed' => $totalborrowed]);
    }

    public function searchBorrowed(Request $request){
        if($request->daterange == '' || $request->daterange[0] == null){
            $borrowed = BorrowedBook::whereHas('patron', function ($query){
                $query->where('first_name', 'like', '%'.request()->get('search').'%')
                ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
            })->orWhereHas('book', function ($query){
                $query->where('name', 'like', '%'.request()->get('search').'%');
            })->with(['patron', 'book', 'book.category'])->paginate(10);
        }
        else if($request->search == ''){
            $borrowed = BorrowedBook::whereBetween('returndate', [$request->daterange[0], $request->daterange[1]])
            ->with(['patron', 'book', 'book.category'])->paginate(10);
        }
        else {
            $borrowed = BorrowedBook::whereBetween('returndate', [$request->daterange[0], $request->daterange[1]])->whereHas('patron', function ($query){
                $query->where('first_name', 'like', '%'.request()->get('search').'%')
                ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
            })->orWhereHas('book', function ($query){
                $query->where('name', 'like', '%'.request()->get('search').'%');
            })->with(['patron', 'book', 'book.category'])->paginate(10);
        }

        return response()->json($borrowed);
    }

}
