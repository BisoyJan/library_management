<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class LibrarianAccountController extends Controller
{
    public function index() {
        return response()->json(User::where('type', '<>', 'Admin')->paginate(10));
    }

    public function setAccountType(Request $request){
        $user = User::find($request->id);
        if($user->type == 'Pending' || $user->type == 'pending') {
            $user->update(['type' => 'Approved']);
        } else {
            $user->update(['type' => 'Pending']);
        }
        return response()->json(User::where('type', '<>', 'Admin')->paginate(10));
    }

    public function searchLibrarian(Request $request){
        $librarian = User::where('fname', 'like', '%'.$request->search.'%')
        ->orWhere('mname', 'like', '%'.$request->search.'%')
        ->orWhere('lname', 'like', '%'.$request->search.'%')
        ->where('type', '<>', 'Admin')->paginate(10);

        return response()->json($librarian, 200);
    }
}
