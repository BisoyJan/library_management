<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['store', 'login']]);
    }

    public function store(UserRequest $request)
    {
        $data = [
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'fname' => $request->fname,
            'mname' => $request->mname,
            'lname' => $request->lname,
            'contact_number' => $request->contact_number,
            'gender' => $request->gender
        ];

        if($request->type){
            $data['type'] = $request->type;
        };

        User::create($data);

        return response()->json(['success' => 'Account created successfuly!'], 200);
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        $user = User::where('email', $request->email)->first();


        if($user->type <> 'Pending'){
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
        }
        else {
            return response()->json(['error' => 'Account is pending for approval'], 401);
        }
        


        return $this->respondWithToken($token);
    }

    public function update(Request $request) {
        try {
            $data = [
                'fname' => $request->fname,
                'mname' => $request->mname,
                'lname' => $request->lname,
                'gender' => $request->gender,
                'contact_number' => $request->contact_number,
                'email' => $request->email,
                'type' => $request->type
            ];

            if($request->password) {
                $data['password'] = Hash::make($request->password);
            }

            $book = User::find($request->id);

            $book->update($data);

           return response()->json(['message' => 'User updated successfully!'], 200);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'User not found'], 404);
        }
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        JWTAuth::invalidate(Request()->token);
        auth()->logout();
        return response()->json(['message' => 'User logged out successfully!']);
    }

    /**
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
}
