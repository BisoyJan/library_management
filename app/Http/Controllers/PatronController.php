<?php

namespace App\Http\Controllers;

use App\Http\Requests\PatronRequest;
use App\Models\Patron;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return response()->json(Patron::paginate(10));
    }

    public function getAllPatrons()
    {
        return response()->json(Patron::all());
    }

    public function store(PatronRequest $request)
    {
        return response()->json(Patron::create($request->validated()));
    }

    public function show($id)
    {
        try {
            return response()->json(Patron::findOrFail($id));
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    public function update(PatronRequest $request, $id)
    {
        try {
            $patron = Patron::findOrFail($id);
            $patron->update($request->validated());

            return response()->json(['message' => 'Patron updated', 'patron' => $patron]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    public function destroy($id)
    {
        try {
            $patron = Patron::where('id', $id)->firstOrFail();
            $patron->delete();

            return response()->json(['message' => 'Patron deleted successfully!']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    public function searchPatron(Request $request){
        $librarian = Patron::where('first_name', 'like', '%'.$request->search.'%')
        ->orWhere('middle_name', 'like', '%'.$request->search.'%')
        ->orWhere('last_name', 'like', '%'.$request->search.'%')
        ->paginate(10);

        return response()->json($librarian, 200);
    }
}
