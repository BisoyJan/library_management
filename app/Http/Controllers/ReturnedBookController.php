<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnedBookRequest;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->paginate(10));
    }

    public function store(ReturnedBookRequest $request)
    {
        //Retrieve first the borrowed book
        try {
            $borrowedbook = BorrowedBook::where([
                ['id', $request->borrowed_id],
            ])->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }

        if (!empty($borrowedbook)) {
            if ($borrowedbook->copies == $request->copies) {
                $borrowedbook->delete();
                
            } else {
                $borrowedbook->update(['copies' => $borrowedbook->copies - $request->copies]);
            }

            $create_returned = ReturnedBook::create($request->validated());
            $returnedbook = ReturnedBook::with(['book'])->find($create_returned->id);
            $copies = $returnedbook->book->copies + $request->copies;
            $borrowedbook = BorrowedBook::where([
                ['id', $request->borrowed_id],
            ])->first();    
            $returnedbook->book->update(['copies' => $copies]);
            return response()->json(['message' => 'Book returned successfully!', 'book' => $borrowedbook]);
        }
    }

    public function show($id)
    {
        try {
            $returnedbook = ReturnedBook::with(['book', 'book.category', 'patron'])->findOrfail($id);
            return response()->json($returnedbook);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Returned book not found'], 404);
        }
    }

    public function summary(){

        $due = BorrowedBook::whereDate('returndate', Carbon::now('Asia/Manila')->format('Y-m-d'))->count();
        $totalreturned = ReturnedBook::count();
        return response()->json(['due' => $due, 'totalreturned' => $totalreturned]);
    }

    public function searchReturned(Request $request){
        $borrowed = ReturnedBook::whereHas('patron', function ($query){
            $query->where('first_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
        })->orWhereHas('book', function ($query){
            $query->where('name', 'like', '%'.request()->get('search').'%');
        })->with(['patron', 'book', 'book.category'])->paginate(10);

        return response()->json($borrowed);
    }
}
