<?php

namespace App\Http\Requests;

use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = BorrowedBook::where('id', request()->get('borrowed_id'))->first();

        if (!empty($borrowed)) {
            $copies = $borrowed->copies;
        } else {
            $copies = request()->get('copies');
        }

        return [
            'book_id' => 'bail|required|exists:borrowed_books,book_id',
            'copies' => ['gt:0', "lte: {$copies}", 'required', 'bail'],
            'patron_id' => 'exists:borrowed_books,patron_id',
            'borroweddate' => 'required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
