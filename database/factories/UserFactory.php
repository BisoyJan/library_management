<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fname' => $this->faker->firstName,
            'mname' => $this->faker->lastName,
            'lname' => $this->faker->lastName,
            'email' => $this->faker->unique()->safeEmail,
            'gender' => 'Male',
            'contact_number' => '0912345',
            'password' => Hash::make('123123'),
        ];
    }
}
