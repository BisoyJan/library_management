<?php

namespace Database\Seeders;

use App\Models\Book;
use Illuminate\Database\Seeder;

class BooksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            [
                'name' => 'C++ Fundamentals',
                'author' => 'Batman',
                'copies' => rand(25, 300),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Chronicles Volume 1',
                'author' => 'Bob Dylan',
                'copies' => rand(90, 500),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Java Fundamentals',
                'author' => 'Superman',
                'copies' => rand(25, 300),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Web Basics',
                'author' => 'Flash',
                'copies' => rand(25, 300),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'A Little Life',
                'author' => 'Hanya Yanagihara',
                'copies' => rand(70, 500),
                'category_id' => rand(1, 5),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Python ML',
                'author' => 'Batman',
                'copies' => rand(25, 300),
                'category_id' => rand(3, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Dark Mans',
                'author' => 'Nicola Barker',
                'copies' => rand(25, 300),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Maria Clara',
                'author' => 'Batman',
                'copies' => rand(25, 300),
                'category_id' => rand(6, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Light',
                'author' => 'M John Harrison',
                'copies' => rand(125, 600),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Dark Side',
                'author' => 'Superman',
                'copies' => rand(25, 300),
                'category_id' => rand(2, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Broken Glass',
                'author' => 'Alain Mabanckou',
                'copies' => rand(50, 300),
                'category_id' => rand(4, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Harry Potter and the Goblet of Fire',
                'author' => 'JK Rowling',
                'copies' => rand(50, 300),
                'category_id' => rand(4, 8),
                'description' => 'Book Description goes here.'
            ],
            [
                'name' => 'Bad Blood',
                'author' => 'Lorna Sage',
                'copies' => rand(70, 300),
                'category_id' => rand(1, 8),
                'description' => 'Book Description goes here.'
            ],
        ];

        foreach ($books as $book) {
            Book::create($book);
        }
    }
}
