<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
     public function run()
    {
        $category = [
            ['category' => 'Adventure'],
            ['category' => 'Horror'],
            ['category' => 'Novel'],
            ['category' => 'History'],
            ['category' => 'Fantasy'],
            ['category' => 'Fiction'],
            ['category' => 'Mystery'],
            ['category' => 'Programming'],
        ];

        foreach ($category as $categ) {
            Category::create($categ);
        }
        
    }
}