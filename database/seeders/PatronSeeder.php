<?php

namespace Database\Seeders;

use App\Models\Patron;
use Illuminate\Database\Seeder;

class PatronSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patrons = [
            [
                'first_name' => 'Genreve',
                'middle_name' => 'Peque',
                'last_name' => 'Fernandez',
                'gender' => 'Female',
                'email' => 'genrevepequefernandez@gmail.com',
                'contact_number' => '0912345678',
            ],
            [
                'first_name' => 'Yvan',
                'middle_name' => 'Caindoy',
                'last_name' => 'Sabay',
                'gender' => 'Male',
                'email' => 'sabayyvan2018@gmail.com',
                'contact_number' => '0912345678',
            ],
            [
                'first_name' => 'Jan Ramil',
                'middle_name' => 'Pantorilla',
                'last_name' => 'Intong',
                'gender' => 'Male',
                'email' => 'bisoyjan@gmail.com',
                'contact_number' => '0912345678',
            ],
            [
                'first_name' => 'Danica',
                'middle_name' => 'Fuentes',
                'last_name' => 'Barrientos',
                'gender' => 'Female',
                'email' => 'averybering@gmail.com',
                'contact_number' => '0912345678',
            ],
        ];

        foreach ($patrons as $patron) {
            Patron::create($patron);
        }
    }
}
