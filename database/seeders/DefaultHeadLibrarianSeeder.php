<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DefaultHeadLibrarianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'fname' => 'Head',
            'mname' => 'Librarian',
            'lname' => 'Account',
            'gender' => 'Male',
            'email' => 'admin@admin.com',
            'contact_number' => '093512345678',
            'password' => Hash::make('admin123'),
            'type' => 'Admin'
        ]);
    }
}
