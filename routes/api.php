<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LibrarianAccountController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\ReturnedBookController;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('store', [AuthController::class, 'store']);
    Route::put('update', [AuthController::class, 'update']);
});

Route::group(['middleware' => 'api'], function () {

    Route::apiResource('patrons', PatronController::class);
    Route::post('patrons/searchPatron', [PatronController::class, 'searchPatron']);
    Route::get('all/patron', [PatronController::class, 'getAllPatrons']);

    Route::apiResource('books', BookController::class);
    Route::post('books/searchBook', [BookController::class, 'searchBook']);

    Route::apiResource('borrowedbook', BorrowedBookController::class);
    Route::post('borrowedbook/searchBorrowed', [BorrowedBookController::class, 'searchBorrowed']);
    Route::get('borrowedsummary', [BorrowedBookController::class, 'summary']);
    
    Route::apiResource('/returnedbook', ReturnedBookController::class);
    Route::get('returnedsummary', [ReturnedBookController::class, 'summary']);
    Route::post('returnedbook/searchReturned', [ReturnedBookController::class, 'searchReturned']);

    Route::apiResource('/categories', CategoryController::class);

    Route::get('/latest', [DashboardController::class, 'getLatestData']);
    
    Route::group(['middleware' => 'jwt.admin'], function () {
        Route::get('/librarian/accounts', [LibrarianAccountController::class, 'index']);
        Route::post('/librarian/searchLibrarian', [LibrarianAccountController::class, 'searchLibrarian']);
        Route::post('/setType', [LibrarianAccountController::class, 'setAccountType']);
    });
});
